package com.bang.middle.server.controller;

import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author qinxubang
 * @Date 2021/4/23 17:11
 */
@RestController
public class HelloController {

    /**
     * Controller控制层各个参数解释
     *  对于get请求， @RequestParam 请求参数
     *  对于post请求，@RequestBody 请求体
     *
     *
     * @param str
     * @return
     */

    @RequestMapping(value = "/test/demo", method = RequestMethod.GET)
    public String test(@RequestParam String str) {
        return "你好，" + str;
    }


    @RequestMapping(value = "/test/demo2", method = RequestMethod.GET)
    public String test2() {
        return "你好";
    }


}
