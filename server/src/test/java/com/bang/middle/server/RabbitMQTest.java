package com.bang.middle.server;

import com.bang.middle.server.rabbitmq.publisher.BasicPublisher;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author qinxubang
 * @Date 2021/4/23 15:34
 */
@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class RabbitMQTest {

    @Autowired
    private BasicPublisher basicPublisher;

    // 测试 基本消息模型，消息内容为 字符串
    @Test
    public void testBasicMessageModel() {
        String msgStr = "~~~这是一串字符串消息~~~~";
        basicPublisher.sendMsg(msgStr);
    }
}
